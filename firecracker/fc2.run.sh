#!/bin/bash -e

if [ ! -f hello-vmlinux.bin ]; then
  curl -fsSL -o hello-vmlinux.bin https://s3.amazonaws.com/spec.ccfc.min/img/hello/kernel/hello-vmlinux.bin
fi
if [ ! -f hello-rootfs.ext4 ]; then
  curl -fsSL -o hello-rootfs.ext4 https://s3.amazonaws.com/spec.ccfc.min/img/hello/fsfiles/hello-rootfs.ext4
fi

#
# https://github.com/firecracker-microvm/firecracker-demo/blob/master/start-firecracker.sh
#
API_SOCKET="/tmp/firecracker.socket"

CURL=(curl --silent --show-error --header Content-Type:application/json --unix-socket "${API_SOCKET}" --write-out "HTTP %{http_code}")

curl_put() {
    local URL_PATH="$1"
    local OUTPUT RC
    OUTPUT="$("${CURL[@]}" -X PUT --data @- "http://localhost/${URL_PATH#/}" 2>&1)"
    RC="$?"
    if [ "$RC" -ne 0 ]; then
        echo "Error: curl PUT ${URL_PATH} failed with exit code $RC, output:"
        echo "$OUTPUT"
        return 1
    fi
    # Error if output doesn't end with "HTTP 2xx"
    if [[ "$OUTPUT" != *HTTP\ 2[0-9][0-9] ]]; then
        echo "Error: curl PUT ${URL_PATH} failed with non-2xx HTTP status code, output:"
        echo "$OUTPUT"
        return 1
    fi
}

logfile="$PWD/firecracker.log"
#metricsfile="$PWD/output/fc-sb${SB_ID}-metrics"
metricsfile="/dev/null"

rm -f $logfile
touch $logfile

#
# set the guest kernel:
#
curl_put '/boot-source' <<EOF
{
  "kernel_image_path": "./hello-vmlinux.bin",
  "boot_args": "console=ttyS0 reboot=k panic=1 pci=off"
}
EOF

#
# set the guest rootfs:
#
curl_put '/drives/rootfs' <<EOF
{
  "drive_id": "rootfs",
  "path_on_host": "./hello-rootfs.ext4",
  "is_root_device": true,
  "is_read_only": false
}
EOF

curl_put '/logger' <<EOF
{
  "log_fifo": "$logfile",
  "metrics_fifo": "$metricsfile",
  "level": "Warning",
  "show_level": false,
  "show_log_origin": false
}
EOF

#
# set config
#
curl_put '/machine-config' <<EOF
{
  "vcpu_count": 2,
  "mem_size_mib": 1024
}
EOF

#
# start the guest machine:
#
curl_put '/actions' <<EOF
{
  "action_type": "InstanceStart"
}
EOF
