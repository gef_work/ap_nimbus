# `Firecracker` "serverless" compute investigations.

As an alternative to using [Docker](https://www.docker.com/) containers Amazon's serverless compute engine [AWS Fargate](https://aws.amazon.com/fargate/) executes tasks on [Firecracker](https://firecracker-microvm.github.io/) MicroVMS (claiming to ensure rapid start-up and isolation across tasks).

The intention in this activity is to create an `ApPredict` MicroVM with guidance from the blogs by [Jeff Barr](https://aws.amazon.com/blogs/aws/firecracker-lightweight-virtualization-for-serverless-computing) and [Arun Gupta and Linda Lian](https://aws.amazon.com/blogs/opensource/firecracker-open-source-secure-fast-microvm-serverless/). Within the latter is there is the instruction to download a sample kernel and rootfs - so the plan is to use [Alpine Linux's RootFS creator](https://github.com/alpinelinux/alpine-make-rootfs) (v.0.2.0) as the foundation for `ApPredict`.

Note 1: Following instructions done on Fedora 26.

Note 2: `sudo` ability is required for `mount`, `umount`, `chown` operations.

Note 3: Firecracker currently (as of Dec. 2018) only runs on Intel.

## Step 1a. Build (v0.11.0) Firecracker.

(Alternatively download it from [here](https://github.com/firecracker-microvm/firecracker/releases/download/v0.11.0/firecracker-v0.11.0)!)

  1. `git clone https://github.com/firecracker-microvm/firecracker`
  1. `cd firecracker`
  1. `git checkout tags/v0.11.0`
  1. `tools/devtool build --release`
  1. `cd build/release`

This should have created the `firecracker` and `jailer` binaries.

## Step 1b. Check out Alpine Linux's RootFS-maker.

(Alternatively download it from [here](https://raw.githubusercontent.com/alpinelinux/alpine-make-rootfs/v0.2.0/alpine-make-rootfs)!)

  1. `git clone https://github.com/alpinelinux/alpine-make-rootfs`
  1. `cd alpine-make-rootfs`
  1. `git checkout tags/v0.2.0`

Within the cloned structure there's an example of how to use the RootFS maker.

## Step 2. Create Alpine Linux root ext4 filesystem data.

  1. `1.make-alpine-rootfs.sh`
  1. `2.make-rootfs-ext4.sh`  
     This step will copy a compiled `ApPredict` and dependencies if available, e.g. copied from the docker container built in [appredict-no-emulators](https://bitbucket.org/gef_work/ap_nimbus/src/master/appredict-no-emulators/) or [appredict-with-emulators](https://bitbucket.org/gef_work/ap_nimbus/src/master/appredict-with-emulators/).

## Step 3. Run.

You'll need two consoles to see this in action!

  1. In the first console: `fc1.listen.sh`  
     (Assuming the `firecracker` binary is in your `PATH`) This leaves firecracker in the waiting (listening at socket) state - it'll change when the next step is run.
  1. In the second console: `fc2.run.sh`  
     This should cause the first console to show the start up of the `ApPredict` MicroVM (log in via 'root' or 'appredict' and no password (or with 'root' password if using the Amazon rootfs)).  
     It'll download any missing required file!


