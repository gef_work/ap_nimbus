#!/bin/bash -e

if [ ! -d tmp ]; then
  mkdir tmp
fi
pushd tmp
rm -rf *
tar -zxvf ../output/rootfs.tar.gz

# Derived from docker appredict 0.0.1 image (e.g. `docker cp <container id>:/home/. .`)
if [ -d ../appredict ]; then
  echo ""
  echo "  Please wait while copying /home/appredict/ derived from docker image"
  echo ""
  mkdir -p home/appredict
  cp -r ../appredict home/
fi

echo "ttyS0" >> etc/securetty

# Derived from docker appredict 0.0.1 image.
echo "appredict:x:10101:appredict" >> etc/group
echo "appredict:x:10101:10101:Linux User,,,:/home/appredict:/bin/bash" >> etc/passwd
echo "appredict::123456:0:99999:7:::" >> etc/shadow

pushd etc/init.d
ln -sv agetty agetty.ttyS0
popd

pushd etc/runlevels
pushd boot
ln -sv /etc/init.d/{devfs,hostname,procfs,sysfs} .
popd
pushd default
ln -sv /etc/init.d/agetty.ttyS0 .
popd
pushd nonetwork
ln -sv /etc/init.d/agetty.ttyS0 .
popd
popd

popd

if [ ! -d mounted ]; then
  mkdir mounted
fi

rm -f rootfs.ext4

# 262144 == 1Gb
dd if=/dev/zero of=rootfs.ext4 bs=4096 count=1572864
mkfs.ext4 -v rootfs.ext4
e2label rootfs.ext4 ROOTFS
sudo mount -o loop rootfs.ext4 mounted
pushd mounted
sudo cp -ra ../tmp/* .
if [ -d home/appredict ]; then
  sudo chown -R 10101:10101 home/appredict
fi
sync
popd
sudo umount mounted
