#!/bin/bash -e

os=$(awk -F= '/^NAME/{print $2}' /etc/os-release)

if [ "${os}" == "Fedora" ]; then
  if [ ! -L /etc/ssl/cert.pem ]; then
    # see https://github.com/postmarketOS/pmbootstrap/issues/722
    echo ""
    echo "  On Fedora you probably first need to run (as root) 'ln -sf /etc/pki/tls/cert.pem /etc/ssl/cert.pem'!"
    echo ""
    exit 1
  fi
fi

if [ -f alpine-make-rootfs ]; then
  rm -f alpine-make-rootfs
fi

wget https://raw.githubusercontent.com/alpinelinux/alpine-make-rootfs/v0.2.0/alpine-make-rootfs \
     && echo 'f290d9f09395b107e570dbe2eb1c5bad98db1cbb  alpine-make-rootfs' | sha1sum -c \
     || exit 1
patch -p0 alpine-make-rootfs < alpine-make-rootfs.patch
chmod 700 alpine-make-rootfs

if [ -d output ]; then
  rm -rf output/*
else
  mkdir -p output
fi

# gef_work ap_nimbus/appredict/Dockerfile
# openrc required by firecracker
sudo ./alpine-make-rootfs --branch v3.8 --packages 'bash g++ git libexecinfo-dev libxslt-dev nano openrc python-dev scons util-linux wget' --script-chroot output/rootfs.tar.gz -- ./do_nothing.sh

